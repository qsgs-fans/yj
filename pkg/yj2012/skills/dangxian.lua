local dangxian = fk.CreateSkill {
  name = "dangxian"
}

Fk:loadTranslationTable{
  ['dangxian'] = '当先',
  [':dangxian'] = '锁定技，回合开始时，你执行一个额外的出牌阶段。',
  ['$dangxian1'] = '先锋就由老夫来当！',
  ['$dangxian2'] = '看我先行破敌！',
}

dangxian:addEffect(fk.EventPhaseChanging, {
  anim_type = "offensive",
  frequency = Skill.Compulsory,
  can_trigger = function(skill, event, target, player, data)
    return target == player and player:hasSkill(dangxian.name) and data.to == Player.Start
  end,
  on_use = function(skill, event, target, player, data)
    player:gainAnExtraPhase(Player.Play, true)
  end,
})

return dangxian